# PosterFinder App

> App that finds Poster and Backdrops using React and the TMDB API.

## The MovieDB API

Get your API key and add to the .ENV file

[https://developers.themoviedb.org/3](https://developers.themoviedb.org/3)

## Quick Start

```bash
# Install dependencies
npm install

# Serve on localhost:3000
npm start

# Build for production
npm run build
```

## App Info

### Author

Cornano Ronan

### Version

1.0.0

### License

This project is licensed under the MIT License
