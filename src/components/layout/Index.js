import React from 'react';
import Tracks from '../tracks/Tracks';
import Search from '../tracks/Search';
const Index = () => {
  return (
    <React.Fragment>
      <div class="col-12"><div class="container"><Search /></div></div>
      <div class="col-10 "><Tracks /></div>
    </React.Fragment>
  );
};

export default Index;
