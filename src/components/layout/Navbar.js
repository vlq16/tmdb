import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="navbar" style={{background: '#CC0000!important'}}>
      <Link to="/">
        <span className="navbar-brand text-white">Poster Finder</span>
      </Link>
    </nav>
  );
};

export default Navbar;
