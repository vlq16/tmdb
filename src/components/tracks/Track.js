import React from 'react';
import { Link } from 'react-router-dom';

const Track = props => {
  const { track, mediaType } = props;
  const baseThumb = "https://image.tmdb.org/t/p/w200/";



  return (
    <div className="col-sm-12 col-md-12 col-lg-12 col-xl-5">
      <div class="card">
        <div class="row">
          <div class="col-md-4">
            <img className="card-img" id={track.id} alt={track.original_title} src={baseThumb+track.poster_path} />
          </div>
          <div class="row col-md-8 align-items-center">
            <div class="card-body ">
              <h5 class="card-title text-center ">{track.title || track.name}</h5>
                <Link
                  to={`${mediaType}/info/${track.id}`}
                  className="btn btn-dark btn-block stretched-link"
                >
                  <i className="fas fa-film" /> Voir infos
                </Link>
            </div>
          </div>
        </div>
      </div>
  </div>


  );
};

export default Track;
