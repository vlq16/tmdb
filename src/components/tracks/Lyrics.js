import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Spinner from '../layout/Spinner';
import Moment from 'react-moment';
import Accordion from 'react-accordion-feature';
import { ColorExtractor } from 'react-color-extractor';
class Lyrics extends Component {

  state = {
    track: {},
    lyrics: {},
    colors: [],
    casts: [],
    crews: [],
    first: ''
  };

  componentDidMount() {


    const first = this.props.location.pathname.split("/")[1];
    this.setState({ first: first });

    // REQUEST FOR TV
    if (first === 'tv') {
      axios
        .get(
          `https://api.themoviedb.org/3/${first}/${this.props.match.params.id}/images?api_key=${process.env.REACT_APP_MM_KEY}`
        )
        .then(res => {
          this.setState({ lyrics: res.data });
          return axios.get(
            `https://api.themoviedb.org/3/${first}/${this.props.match.params.id}?api_key=${process.env.REACT_APP_MM_KEY}&language=fr-FR`
          );
        })
        .then(res => {
          this.setState({ track: res.data });
          return axios.get(
            `http://api.themoviedb.org/3/${first}/${this.props.match.params.id}/credits?api_key=${process.env.REACT_APP_MM_KEY}&language=fr-FR`
          );
        })
        .then(res => {
          const theCrew = [];
          const theCast = [];
          for (var i = 0; i < 6; i++) {
            theCrew.push(res.data.crew[i]);
            theCast.push(res.data.cast[i]);
          }
          this.setState({ casts: theCast, crews: theCrew });
        })
        .catch(err => console.log(err));
    }
    // REQUEST FOR MOVIE
    else {
      axios
        .get(
          `https://api.themoviedb.org/3/${first}/${this.props.match.params.id}/images?api_key=${process.env.REACT_APP_MM_KEY}`
        )
        .then(res => {
          this.setState({ lyrics: res.data });
          return axios.get(
            `https://api.themoviedb.org/3/${first}/${this.props.match.params.id}?api_key=${process.env.REACT_APP_MM_KEY}&language=fr-FR`
          );
        })
        .then(res => {
          this.setState({ track: res.data });
          return axios.get(
            `http://api.themoviedb.org/3/${first}/${this.props.match.params.id}/casts?api_key=${process.env.REACT_APP_MM_KEY}&language=fr-FR`
          );
        })
        .then(res => {
          const theCrew = [];
          const theCast = [];
          for (var i = 0; i < 6; i++) {
            theCrew.push(res.data.crew[i]);
            theCast.push(res.data.cast[i]);
          }
          this.setState({ casts: theCast, crews: theCrew });
        })
        .catch(err => console.log(err));
    }

  }
  renderSwatches = () => {
    const { colors } = this.state

    return colors.map((color, index) => {
      return (
        <div
          title={color}
          key={index}

          style={{
            backgroundColor: color,
            width: 50,
            height: 50,
            title: color,
            display: 'block',
            margin: 'auto'
          }}
        />
      )
    })
  }

  getColors = colors => this.setState(state => ({ colors: [...state.colors, ...colors] }));

  render() {

    const { track, lyrics, casts, crews, colors } = this.state;
    const firstColors = colors[Math.floor(Math.random() * colors.length)];
    const baseThumb = "https://image.tmdb.org/t/p/w200";
    const baseImg = "https://image.tmdb.org/t/p/original";

    if (
      track === undefined ||
      crews === undefined ||
      lyrics === undefined ||
      casts === undefined ||
      colors === undefined ||
      Object.keys(track).length === 0 ||
      Object.keys(lyrics).length === 0 ||
      Object.keys(crews).length === 0 ||
      Object.keys(casts).length === 0
    ) {
      return <Spinner />;
    } else {
      console.log('lyrics', lyrics);
      console.log('track', track);
      console.log('casts', casts);
      console.log('crews', crews);
      console.log('colors', colors);
      console.log('firstColors', firstColors);
      console.log(this.state);
      return (
        <React.Fragment>
          <div id="headerLyrics" className="col-12" style={{ backgroundColor: firstColors || '#4e0806'}}>
            <Link to="/" className=" justify-content-left btn btn-success btn-sm mb-4">Go Back</Link>
            <div className="container">
              <div className="row">

                <div className="col-lg-4 col-sm-12">

                  <ColorExtractor maxColors={256} getColors={this.getColors}>
                    <img className="col img-fluid" alt={track.original_name} src={baseImg + track.poster_path} />
                  </ColorExtractor>

                  <div
                    style={{
                      display: 'flex',
                      border: 'solid 1px black',
                      backgroundColor: "white"
                    }}
                    className="align-self-center">
                    {this.renderSwatches()}
                  </div>

                </div>

                <div className="col-lg-8 card-body card-header">

                  <h1>{track.original_title || track.original_name}</h1>
                  <strong>Date de sortie</strong> :{' '}
                  <Moment format="DD/MM/YYYY">{track.release_date}</Moment>
                  <h5 className="card-title">Synopsis</h5>
                  <p className="card-text">{track.overview}</p>
                
                  {track.hasOwnProperty('belongs_to_collection') && track.belongs_to_collection !== null ?
                    <Link to={`/saga/info/${track.belongs_to_collection.id}`} className="stretched-link" >
                     <div> <h1>{track.belongs_to_collection.name}</h1>
                      <img className="col img-fluid" src={baseImg + track.belongs_to_collection.backdrop_path} /></div>
                    </Link> : null
                  }

                </div>
                
                <div className="col-12 ">
                  <div className="card-header col-12 ">
                  <h5 className="justify-content-left card-title col-12">Studio</h5>
                      <div className="row align-items-center">

                        {track.production_companies.map((item) => (item ? (
                          <div key={item.id} className="col-2">
                            <img className="img-fluid img-thumbnail" alt={item.name} src={baseThumb + item.logo_path} onError={(e) => { e.target.onerror = null; e.target.style = "display:none" }} />
                          </div>
                        ) : null
                        ))}
                        {track.hasOwnProperty('networks') && track.belongs_to_collection !== null ? 
                          track.networks.map((item) => (item ? (
                            <div key={item.id} className="col-2">
                              <img className="img-fluid img-thumbnail" alt={item.name} src={baseThumb + item.logo_path} onError={(e) => { e.target.onerror = null; e.target.style = "display:none" }} />
                            </div>
                          ) : null
                        )): null }

                      </div>
                  </div>

                  <div className="col-12  card-body row card-header">

                    <h5 className="justify-content-left card-title col-12">Équipe technique en vedette</h5>

                    {crews.map((item) => (item ? (

                      <div key={item.id} className="col align-items-center">
                        <img className="img-fluid img-thumbnail" alt={item.name} src={baseThumb + item.profile_path} onError={(e) => { e.target.onerror = null; e.target.style = "display:none" }} />
                        <p className="card-text">
                          {item.name}
                          <br></br>
                          {item.job}
                        </p>
                      </div>

                    ) : null
                    ))}

                  </div>

                </div>
              </div>
            </div>
            <div className="container">
              <div className="card-body row card-header">
                <h5 className="justify-content-left card-title col-12">Tête d'affiche</h5>
                {casts.map((item) => (item ? (
                  <div key={item.id} className="col align-items-center">
                    <Link
                      to={`/person/info/${item.id}`}
                      className="stretched-link"
                    >

                      <img className="img-fluid img-thumbnail" alt={item.name} src={baseThumb + item.profile_path} onError={(e) => { e.target.onerror = null; e.target.style = "display:none" }} />
                      <p className="card-text text-white">
                        {item.name}
                        <br></br>
                        {item.character}
                      </p>
                    </Link>
                  </div>
                ) : null
                ))}
              </div>
            </div>
          </div>

          <Accordion customClass="accordionWrapper container">
            <Accordion.Pane title="Poster">
              <div className="row">
                {lyrics.posters.map((item, index) => (item ? (
                  <div key={index} className="card col-md-3 shadow-sm">
                    <a href={baseImg + item.file_path} target="_blank"><img className="card-img-top img-fluid img-thumbnail" alt={track.original_title} src={baseThumb + item.file_path} /></a>
                  </div>
                ) : null
                ))}
              </div>
            </Accordion.Pane>

            <Accordion.Pane title="Backdrops">
              <div className="row">
                {lyrics.backdrops.map((item, index) => (item ? (
                  <div key={index} className="card col-md-3 shadow-sm">
                    <a href={baseImg + item.file_path} target="_blank"><img className="card-img-top img-fluid img-thumbnail" alt={track.original_title} src={baseThumb + item.file_path} /></a>
                  </div>
                ) : null
                ))}
              </div>
            </Accordion.Pane>
          </Accordion>

          <br></br>

          <div className="col-12">

            <Link to="/" className="btn btn-dark btn-sm mb-4">
              Go Back
          </Link>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Lyrics;
