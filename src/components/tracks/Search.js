import React, { Component } from 'react';
import axios from 'axios';
import { Consumer } from '../../context';

class Search extends Component {

  constructor(props) {
    super(props);

    this.state = {
      trackType: 'movie',
      trackTitle: '',
      isGoing: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }

  handleChangeType(e) {
     this.setState({trackType: e.target.value});
   }
  handleChange(e) {
     const target = e.target;
     const value = target.type === 'checkbox' ? target.checked : target.value;
     const name = target.name;

     this.setState({
       [name]: value
     });
   }

   handleSubmit(e) {
     e.preventDefault();
   }

  findTrack = (dispatch, e) => {
    e.preventDefault();

    axios
      .get(
        `https://api.themoviedb.org/3/search/${
          this.state.trackType
        }?page=1&include_adult=${
          this.state.isGoing
        }&query=${
          this.state.trackTitle
        }&api_key=${
          process.env.REACT_APP_MM_KEY
        }`
      )
      .then(res => {
        console.log('res',res);
        dispatch({
          type: 'SEARCH_TRACKS',
          payload: res.data.results,
          trackType: this.state.trackType,
        });
        this.setState({ trackTitle: '',});
      })
      .catch(err => console.log(err));
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <Consumer>
        {value => {
          const { dispatch } = value;
          return (
            <div id="searchBlock" className="card card-body md-4 p-4">
              <h1 className="display-4 text-center">
                <i class="fas fa-film"></i> Rechercher du contenu
              </h1>
              <p className="lead text-center">Obtenez la vignette pour n'importe quel film ou série</p>
              <form onSubmit={this.findTrack.bind(this, dispatch)}>
                <div className="form-group">
                  <label>
                    Nom du media:
                    <input
                    type="text"
                    className="form-control md-4"
                    placeholder="Titre..."
                    name="trackTitle"
                    value={this.state.trackTitle}
                    onChange={this.onChange}
                  />
                </label>
                <label>
                  Selectionner un type de media:
                  <select value={this.state.value} onChange={this.handleChangeType} className="form-control md-4">
                    <option defaultValue value="movie">Film</option>
                    <option value="tv">Série</option>
                  </select>
                   </label>
                   <label>
                  Adult:
                  <input
                    name="isGoing"
                    type="checkbox"
                    className="form-control md-4"
                    checked={this.state.isGoing}
                    onChange={this.handleChange} />
                </label>
                </div>
                <button
                  className="btn btn-dark btn-block"
                  type="submit"
                >
                  Rechercher
                </button>
              </form>
            </div>
          );
        }}
      </Consumer>
    );
  }
}
export default Search;