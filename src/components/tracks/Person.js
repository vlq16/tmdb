import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Spinner from '../layout/Spinner';
import Moment from 'react-moment';
import Accordion from 'react-accordion-feature';
import { ColorExtractor } from 'react-color-extractor';
class Person extends Component {
  state = {
    person : {},
    myNewArraydataPerson : [],
    dataPerson : [],
    dataPersonNbrPage : ''
    };

  componentDidMount() {


    const first =  this.props.location.pathname.split("/")[1];


    axios
      .get(
        `https://cors-anywhere.herokuapp.com/api.themoviedb.org/3/person/${this.props.match.params.id}/?language=fr-FR&api_key=${process.env.REACT_APP_MM_KEY}`
      )
      .then(res => {
        this.setState({ person: res.data });
        return axios.get(
          `https://cors-anywhere.herokuapp.com/api.themoviedb.org/3/discover/movie/?api_key=${process.env.REACT_APP_MM_KEY}&language=fr-FR&with_cast=${this.props.match.params.id}`
        );
      })
      .then(res => {
        this.setState({ dataPersonNbrPage: res.data.total_pages });
        const { dataPersonNbrPage, dataPerson } = this.state;

        dataPerson.push(res.data.results);


          console.log('dataPersonNbrPage',dataPersonNbrPage);

          for(var i = 1 ; i <= dataPersonNbrPage; i++){
            console.log('i',i);
            axios.get(
              `https://cors-anywhere.herokuapp.com/api.themoviedb.org/3/discover/movie/?api_key=${process.env.REACT_APP_MM_KEY}&page=`+i+`&language=fr-FR&with_cast=${this.props.match.params.id}`
            ).then(res => {
              dataPerson.push(res.data.results) ;
            }).catch(err => console.log(err));
          }
          this.setState({ discoverPerson: dataPerson });

      })
      .catch(err => console.log(err));

  }


  render() {
    const { person, discoverPerson, dataPersonNbrPage, myNewArraydataPerson } = this.state;
    const baseThumb = "https://image.tmdb.org/t/p/w200";
    const baseImg = "https://image.tmdb.org/t/p/original";

    if (

      person === undefined ||
      discoverPerson === undefined ||
      Object.keys(person).length === 0 ||
      Object.keys(discoverPerson).length === 0
    ) {
      return <Spinner />;
    } else {
      let concatAndDeDuplicate = (...arrs) => [ ...new Set( [].concat(...arrs) ) ];

      console.log(concatAndDeDuplicate(discoverPerson));
      console.log("discoverPerson",discoverPerson);

      for (var i = 0; i < discoverPerson.length; ++i) {
        for (var j = 0; j < discoverPerson[i].length; ++j)
        myNewArraydataPerson.push(discoverPerson[i][j]);
      }

      console.log('myNewArraydataPerson', myNewArraydataPerson);
      return (
        <React.Fragment>
          <div className="container">
          <Link to="/" className=" justify-content-left btn btn-success btn-sm mb-4">Go Back</Link>

          <div className="col-12">
            <div  className="row">
              <div  className="col-md-4 col-xs-12 justify-content-center">
                <img className="img-fluid img-thumbnail"  src={baseThumb+person.profile_path}  onError={(e)=>{e.target.onerror = null; e.target.style="display:none"}} />
              </div>
              <div className="col-md-8 col-xs-12">
                <div className="row">
                  <h2 className="col-12">
                    {person.name}
                  </h2>
                  <p className="col-12">
                    {person.biography}
                  </p>
                </div>
              </div>
            </div>

              <div  className="row">

                  <h2 className="col-12">filmographie</h2>

                { myNewArraydataPerson.map(item => (
                  <div className="col-6 card">
                    <div className="row">
                      <div className="col-md-4">
                        <img className="card-img" id={item.id} alt={item.original_title} src={baseThumb+item.poster_path} />
                      </div>
                      <div className="row col-md-8 align-items-center">
                        <div className="card-body ">
                          <h5 className="card-title text-center ">{item.title || item.name}</h5>
                            <Link
                              to={`/movie/info/${item.id}`}
                              className="btn btn-dark btn-block stretched-link"
                            >
                              <i className="fas fa-film" /> Voir infos
                            </Link>
                        </div>
                      </div>
                    </div>
                  </div>
              ))}
              </div>
            </div>

            <Link to="/" className="btn btn-dark btn-sm mb-4">
              Go Back
            </Link>
          </div>
        </React.Fragment>
      );
    }
  }
}

export default Person;
