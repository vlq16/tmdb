import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Index from './components/layout/Index';
import Lyrics from './components/tracks/Lyrics';
import Person from './components/tracks/Person';

import './App.css';

import { Provider } from './context';

class App extends Component {


  render() {
    const supportsHistory = 'pushState' in window.history
    return (
      <Provider>
        <Router forceRefresh={!supportsHistory}>
          <React.Fragment>
            <Navbar />
            <div id="firstRow" className="row justify-content-center">
              <Switch>
                <Route exact path="/" component={Index} />
                <Route exact path="/movie/info/:id" component={Lyrics} />
                <Route exact path="/tv/info/:id" component={Lyrics} />
                <Route exact path="/person/info/:id" component={Person} />
              </Switch>
            </div>
          </React.Fragment>
        </Router>
      </Provider>
    );
  }
}

export default App;
