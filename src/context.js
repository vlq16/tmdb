import React, { Component } from 'react';
import axios from 'axios';

const Context = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case 'SEARCH_TRACKS':
      return {
        ...state,
        track_list: action.payload,
        trackType:action.trackType,
        heading: 'Résultats de la recherche'
      };
    default:
      return state;
  }
};

export class Provider extends Component {
  state = {
    track_list: [],
    trackType: '',
    heading: '',
    dispatch: action => this.setState(state => reducer(state, action))
  };

  componentDidMount() {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?language=fr-FR&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&api_key=${
          process.env.REACT_APP_MM_KEY
        }`
      )
      .then(res => {
        console.log(res.data);
        this.setState({ track_list: res.data.results, trackType:"movie", heading: 'Films les plus populaires',});
      })
      .catch(err => console.log(err));

  }

  render() {
    return (
      <Context.Provider value={this.state}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
